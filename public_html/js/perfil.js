window.addEventListener("load", iniciarValidate);

var verificarModUser = true;
var verificarModPass = true;
var verificarUser = true;
var verificarMovil = true;
var verificarFecha = true;

function iniciarValidate() {
    boton = document.getElementById("confEdit"); 
    boton.addEventListener("click", validacionModUser);
    boton2 = document.getElementById("validar"); 
    boton2.addEventListener("click", validacionModPass);
    expUser = /^[a-zA-ZÑñÁáÉéíÍóÓúÚüñU\s]+$/;
    expPsw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
    expMovil = /^[6,7][0-9]{8}$/;
    validarModUser();
    validarModMovil();
    validarFecha();
    validarModPass();
}

function validarModUser() {
    modNombre = document.getElementById("nombre");
    modNombre.oninput = function () {

        if (!modNombre.value) {
            modNombre.className = 'form-input';
        } else if (!expUser.exec(modNombre.value)) {
            modNombre.className = 'error';
            verificarUser = false;
        } else {
            modNombre.className = 'form-input';
            verificarUser = true;
        }
    };
}

function validarModMovil() {
    modMovil = document.getElementById("movil");
    modMovil.oninput = function() {
        if (!modMovil.value) {
            modMovil.className='form-input';
        }
        else if (!expMovil.exec(movil.value)) {
        modMovil.className='error';
        verificarMovil = false;
        }
        else {
            modMovil.className='form-input';
            verificarMovil = true;
        }
    };
}

function validarFecha() {
    fechaNac = document.getElementById("fechaNac");
    fechaNac.oninput = function () {
        var fechaAct = Date.now();
        var fecha = Date.parse(document.getElementById("fechaNac").value);
        if (fecha > fechaAct) {
            fechaNac.className = 'error';
            alert("Introduce una fecha válida");
            verificarFecha = false;
        } else {
            fechaNac.className = 'form-input';
        }
    };
}

function validarModPass() {
    Vpassword = document.getElementById("Vpassword");
    Npassword = document.getElementById("Npassword");
    Cpassword = document.getElementById("Cpassword");
    Vpassword.oninput = function () {
        if (!Vpassword.value) {
            Vpassword.className='form-input';
        }
        else if (!expPsw.exec(Vpassword.value)) {
        Vpassword.className='error';
        verificarModPass = false;
        }
        else {
            Vpassword.className='form-input';
            verificarModPass = true;
        }
    };
    Npassword.oninput = function () {
        if (!Npassword.value) {
            Npassword.className='form-input';
        }
        else if (!expPsw.exec(Npassword.value)) {
        Npassword.className='error';
        verificarModPass = false;
        }
        else {
            Npassword.className='form-input';
            verificarModPass = true;
        }
    };
    Cpassword.oninput = function () {
        if (!Cpassword.value) {
            Cpassword.className='form-input';
        }
        else if (!expPsw.exec(Cpassword.value)) {
        Cpassword.className='error';
        verificarModPass = false;
        }
        else {
            Cpassword.className='form-input';
            verificarModPass = true;
        }
    };
}


function validacionModUser() {
    verificarModUser = true;
    modNombre = document.getElementById("nombre");
    modMovil = document.getElementById("movil");
    fecha = document.getElementById("fechaNac");
    genero = document.getElementById("genero");
    
    if (!modNombre.value) {
        verificarModUser = false;    
        alert("Debes introducir un nombre");
    }
        
    if (!modMovil.value) {
        verificarModUser = false;   
        alert("Debes introducir un número de móvil");
    }
    
    if (!fecha.value) {
        verificarModUser = false;   
        alert("Debes introducir una fecha de nacimiento");
    }
    
    if (genero.value === null || genero.value === "") {
        alert("Selecciona una de las opciones");
        genero.focus();
        verificarModUser = false;
    }
    
    if (verificarModUser && verificarUser && verificarMovil 
            && verificarFecha) {
        alert("Modificación datos exitosa");
        cargarModUser();
    }
}

function validacionModPass() { 
    Vpassword = document.getElementById("Vpassword");
    Npassword = document.getElementById("Npassword");
    Cpassword = document.getElementById("Cpassword");
    if (!Vpassword.value || !Npassword.value || !Cpassword.value) {
        alert("Rellena todos los campos");
        verificarModPass = false;
    }
    else if (sessionStorage.Contraseña !== Vpassword.value) {
        alert("Introduce correctamente tu contraseña actual");
        verificarModPass = false;
    } 
    else if (Npassword.value !== Cpassword.value){
            alert("La contraseña nueva no coincide");
            verificarModPass = false;
    }
    else if(!expPsw.exec(Cpassword.value)&&!expPsw.exec(Npassword.value)){
        alert("Las contraseñas nuevas son incorrectas, deben tener \n\
        al menos 6 carasteres con minimo una mayuscula y un numero");
    }
    else {
        cargarModPass();
    }
}

