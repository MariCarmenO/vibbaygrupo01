/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var dataBase = null;


window.addEventListener("load", iniciarBotonArt);

function iniciarBotonArt() {
    boton = document.getElementById("buscar");
    boton.addEventListener("click", cargarCategoriaArt);
}


function cargarCategoriaArt() {
    var active = dataBase.result;
    var data = active.transaction(["datosArticulos"], "readonly");
    var object = data.objectStore("datosArticulos");
    var outerHTML = '';
    var categoriaArti = document.getElementById("listcategoria");

    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {

            if (elements[key].categoria === categoriaArti.value) {

                outerHTML += '\n\
               <tr>\n\
                    <td class=columnasArt1 >' + elements[key].id + '</td>\n\
                    <td class=columnasArt2 id=titulo>' + elements[key].nomArticulo + '</td>\n\
                    <td class=columnasArt3>\n\
                    <button type="button" onclick="irVer(' + elements[key].id + ')">Ver Articulo</button>\n\
               </tr>';

            }
            elements = [];
         
            document.getElementById("elementsBuscar").innerHTML = outerHTML;
        }
    };

}