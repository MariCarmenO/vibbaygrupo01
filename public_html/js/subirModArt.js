/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var dataBase = null;

window.addEventListener("load", iniciarBotonSubir);

function iniciarBotonSubir() {
    boton = document.getElementById("subirImgArtMod");
    boton.addEventListener("click", dropear);
}

function dropear(ev)
{
    if (document.querySelector("#NombreArti").value === '' || document.querySelector("#descripc").value === '' || document.querySelector("#precios").value === '' || document.querySelector("#categoriaArti").value === '') {
        alert("Rellena todo los campos primero y luego inserta la imagen");
    }
    
    ev.preventDefault();
    var arch = new FileReader();
    arch.addEventListener('load', leerFotPer, false);
    arch.onload = function (fileLoadedEvent) {
        var elements = [];
        var id = parseInt(localStorage.getItem("idArt"));
        var active = dataBase.result;
        var data = active.transaction(["datosUsuarios"], "readwrite");
        var object = data.objectStore("datosUsuarios");
        var srcData = fileLoadedEvent.target.result; // <--- data: base64
        object.openCursor().onsuccess = function (e) {
            var result = e.target.result;
            if (result === null) {
                return;
            }
            elements.push(result.value);
            result.continue();

            for (var key in elements) {
                if (elements[key].pEmail === sessionStorage.email && elements[key].id === id ) {


                    var updateData = result.value;

                    updateData.foto = srcData;


                    object.put(updateData);

                }
                elements = [];
            }

        };

    };
    arch.readAsDataURL(ev.dataTransfer.files[0]);


}

function allowDropear(ev)
{
    ev.preventDefault();
}

function leerFotPer(ev) {
    document.getElementById('modificarFotoArt').style.backgroundSize = "100% 100%";
    document.getElementById('modificarFotoArt').style.opacity = "1";
    document.getElementById('modificarFotoArt').style.backgroundImage = "url('" + ev.target.result + "')";
}
