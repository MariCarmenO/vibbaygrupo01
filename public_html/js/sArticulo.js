

window.addEventListener("load", iniciarValidacion);
window.addEventListener("load", fotoIntroducida);

var verificar = true;
var verificarPrecio = true;

function fotoIntroducida() {
    document.getElementById("fotoArticulo").addEventListener("drop", soltarFoto, false);
}

var foto = false;

function soltarFoto(ev) {
    ev.preventDefault();
    foto = true;
}

function iniciarValidacion() {
    indice = document.getElementById("categoriaArt");
    boton = document.getElementById("validarArt");
    boton.addEventListener("click", validacion);
    expPrecio = /^([0-9])*[,]?[0-9]*$/;

    
}

function validarPrecio() {
    precio = document.getElementById("precio");
    precio.oninput = function () {
        if (!precio.value) {
            precio.className='form-input';
            verificarPrecio=false;
        }
        else if (!expPrecio.exec(precio.value)) {
            precio.className='error';
            verificarPrecio = false;
        }
        else {
            precio.className='form-input';
        }
    };
}

function validacion() {
    validarPrecio();
    descripcion = document.getElementById("descrip");
    NombreArt = document.getElementById("NombreArt");
    precio = document.getElementById("precio");
    
    if (!foto) {
        alert("Inserta una foto del articulo ");
        foto.focus();
        verificar = false;
    }
    else if (!NombreArt.value) {
        alert("Inserta un nombre para tu articulo ");
        NombreArt.focus();
        verificar = false;
    }
    else if (!descripcion.value) {
        alert("Introduce una descripción del artículo ");
        descripcion.focus();
        verificar = false;
    }
    else if (!precio.value) {
        alert("Inserta el precio");
        precio.focus();
        verificar = false;
    }
    else if (!expPrecio.exec(precio.value)) {
        alert("Inserta un precio valido.\nPor ejemplo: 20,00");
        precio.focus();
        verificar = false;
    }
    else if (indice.value === null || indice.value === "") {
        alert("Selecciona una de las opciones");
        indice.focus();
        verificar = false;
    }
    if (verificar && verificarPrecio) {
        document.sArticulo.submit();
        location.href = "subirArticulo.html";    
    }
}


