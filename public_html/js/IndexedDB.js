
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var dataBase = null;

function startDB() {
    dataBase = indexedDB.open("vibbay", 1);
    dataBase.onupgradeneeded = function (e) {
        active = dataBase.result;
        vibbay = active.createObjectStore("datosUsuarios", {keyPath: "id", autoIncrement: true});
        vibbay.createIndex('porEmail', 'email', {unique: true});
        vibbay = active.createObjectStore("datosArticulos", {keyPath: "id", autoIncrement: true});

    };
    dataBase.onsuccess = function (e) {

        //alert('Base de datos cargada correctamente');
        cargarDatosUsuario();
        cargarTodoArt();
        cargarBuscarArt();
        mostrarImg();

    };
    dataBase.onerror = function (e) {
        // alert('Error cargando la base de datos');
    };
}

function añadir() {
    var active = dataBase.result;
    var data = active.transaction("datosUsuarios", "readwrite");
    var object = data.objectStore("datosUsuarios");

    var request = object.put({
        nomUsuario: document.querySelector("#user").value,
        email: document.querySelector("#email").value,
        contraseña: document.querySelector("#psw").value,
        numMovil: document.querySelector("#movil").value,
        fotoPerfil: "",
        fechaNac: "",
        genero: ""
    });
    request.onerror = function (e) {
        alert(request.error.name + '\n\n' + request.error.message);
    };
    data.oncomplete = function (e) {
        document.querySelector("#user").value = '';
        document.querySelector("#email").value = '';
        document.querySelector("#psw").value = '';
        document.querySelector("#movil").value = '';
        alert('Objeto agregado correctamente');
    };
}



function mostrarImg() {

    var active = dataBase.result;
    var data = active.transaction(["datosUsuarios"], "readonly");
    var object = data.objectStore("datosUsuarios");


    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].email === sessionStorage.email) {
                var newImage = document.createElement('img');
                newImage.height = '100';
                newImage.width = '100';
                newImage.src = result.value.fotoPerfil;
                document.getElementById("avatar").innerHTML = newImage.outerHTML;
            }
            elements = [];
        }
    };

}


function cargarDatosUsuario() {
    var active = dataBase.result;
    var data = active.transaction(["datosUsuarios"], "readonly");
    var object = data.objectStore("datosUsuarios");

    var nombre = document.getElementById("nombre");
    var movil = document.getElementById("movil");
    var fechaNac = document.getElementById("fechaNac");
    var genero = document.getElementById("genero");
    var fotoPerfil = document.getElementById("fotoPerfil");
    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].email === sessionStorage.email) {
                fotoPerfil.value = result.value.fotoPerfil;
                nombre.value = result.value.nomUsuario;
                movil.value = result.value.numMovil;
                fechaNac.value = result.value.fechaNac;
                genero.value = result.value.genero;

            }
            elements = [];
        }
    };

}


function cargarTodoArt() {
    var active = dataBase.result;
    var data = active.transaction(["datosArticulos"], "readonly");
    var object = data.objectStore("datosArticulos");
    var outerHTML = '';


    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].pEmail === sessionStorage.email) {

                outerHTML += '\n\
                        <tr>\n\
                            <td class=columnasArt1 id=num>' + elements[key].id + '</td>\n\
                            <td class=columnasArt2>' + elements[key].nomArticulo + '</td>\n\
                            <td class=columnasArt3>\n\
                                <button type="button" onclick="irEditar(' + elements[key].id + ')">Editar</button>\n\
                        </tr>';


            }
            elements = [];
            document.getElementById("elementsList").innerHTML = outerHTML;
        }
    };

}


function irEditar(id) {
    var active = dataBase.result;
    var data = active.transaction(["datosArticulos"], "readonly");
    var object = data.objectStore("datosArticulos");

    var request = object.get(parseInt(id));
    var NombreArti = document.getElementById("NombreArti");
    var descripc = document.getElementById("descripc");
    var precios = document.getElementById("precios");
    var categoriaArti = document.getElementById("categoriaArti");

    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result2 = request.result;
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].pEmail === sessionStorage.email && elements[key].id === result2.id) {
                localStorage.idArt = result2.id;
                NombreArti.value = result.value.nomArticulo;
                descripc.value = result.value.descripcion;
                precios.value = result.value.precio;
                categoriaArti.value = result.value.categoria;
                var newImage = document.createElement('img');
                newImage.height = '280';
                newImage.width = '280';
                newImage.src = result.value.foto;
                document.getElementById("fotoArticuloEd").innerHTML = newImage.outerHTML;

            }

            elements = [];

        }

    };

}

function cargarBuscarArt() {
    var active = dataBase.result;
    var data = active.transaction(["datosArticulos"], "readonly");
    var object = data.objectStore("datosArticulos");
    var outerHTML = '';
    

    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].id) {

                outerHTML += '\n\
               <tr>\n\
                    <td class=columnasArt1 >' + elements[key].id + '</td>\n\
                    <td class=columnasArt2 id=titulo>' + elements[key].nomArticulo + '</td>\n\
                    <td class=columnasArt3>\n\
                    <button type="button" onclick="irVer(' + elements[key].id + ')">Ver Articulo</button>\n\
               </tr>';


            }


            elements = [];
            document.getElementById("elementsBuscar").innerHTML = outerHTML;
        }
    };

}

function irVer(id) {
    var active = dataBase.result;
    var data = active.transaction(["datosArticulos"], "readonly");
    var object = data.objectStore("datosArticulos");

    var request = object.get(parseInt(id));
    var outerHTML = '';

    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result2 = request.result;
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].id === result2.id) {
                var newImage = document.createElement('img');
                newImage.height = '280';
                newImage.width = '280';
                newImage.src = result.value.foto;

                outerHTML += '\n\
               <div>\n\
                    \n\ <div id=imagen>' + newImage.outerHTML + '</div>\n\
                    <div id=tituloArt>\n\<label>Titulo:</label>\n\
                            ' + elements[key].nomArticulo + '</div>\n\
                    <div id="descripcion">\n\<label>Decripción:</label>\n\
                            ' + elements[key].descripcion + '</div>\n\
                      <div id="precio">\n\<label>precio:</label>\n\
                            ' + elements[key].precio + '</div>\n\
                    \n\<div id="categ">\n\<label>Categoria:</label>\n\
                            ' + elements[key].categoria + '</div>\n\
               </div>';

            }
            elements = [];
            document.getElementById("texto").innerHTML = outerHTML;
        }
    };

}
