/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var dataBase = null;

window.addEventListener("load", iniciarBotonSubir);

function iniciarBotonSubir() {
    boton = document.getElementById("subirImg");
    boton.addEventListener("click", drop);
}

function drop(ev)
{

    ev.preventDefault();
    var arch = new FileReader();
    arch.addEventListener('load', leerFotPer, false);
    arch.onload = function (fileLoadedEvent) {
        var elements = [];
        var active = dataBase.result;
        var data = active.transaction(["datosUsuarios"], "readwrite");
        var object = data.objectStore("datosUsuarios");
        var srcData = fileLoadedEvent.target.result; // <--- data: base64
        object.openCursor().onsuccess = function (e) {
            var result = e.target.result;
            if (result === null) {
                return;
            }
            elements.push(result.value);
            result.continue();

            for (var key in elements) {
                if (elements[key].email === sessionStorage.email) {
                    
                    var updateData = result.value;
                    updateData.fotoPerfil = srcData;
                    object.put(updateData);

                }
                elements = [];
            }

        };

    };
    arch.readAsDataURL(ev.dataTransfer.files[0]);


}

function allowDrop(ev)
{
    ev.preventDefault();
}

function leerFotPer(ev) {
    document.getElementById('fotoPerfil').style.backgroundSize = "100% 100%";
    document.getElementById('fotoPerfil').style.opacity = "1";
    document.getElementById('fotoPerfil').style.backgroundImage = "url('" + ev.target.result + "')";
}
