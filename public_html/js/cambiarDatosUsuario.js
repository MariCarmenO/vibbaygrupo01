var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

var dataBase = null;

function cargarModUser() {
    var active = dataBase.result;
    var data = active.transaction(["datosUsuarios"], "readwrite");
    var object = data.objectStore("datosUsuarios");

    var nombre = document.getElementById("nombre").value;
    var movil = document.getElementById("movil").value;
    var fechaNac = document.getElementById("fechaNac");

    var genero = document.getElementById("genero");

    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null) {
            return;
        }
        elements.push(result.value);
        result.continue();

        for (var key in elements) {
            if (elements[key].email === sessionStorage.email) {


                var updateData = result.value;

                updateData.nomUsuario = nombre;
                updateData.numMovil = movil;
                updateData.fechaNac = fechaNac.value;
                updateData.genero = genero.value;
                object.put(updateData);
                sessionStorage.nombreUsuario = nombre;  
               
            }
            elements = [];
        }
         data.oncomplete = function (e) {
                    document.querySelector("#nombre").value = '';
                    document.querySelector("#movil").value = '';
                    document.querySelector("#fechaNac").value = '';
                    document.querySelector("#genero").value = '';
                    location.reload();
                    
                };
    };
}
